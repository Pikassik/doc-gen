from docx import Document
from docx.shared import Inches
from docx2pdf import convert
from os import system, unlink
from sys import platform


def main():
    document = Document()

    document.add_heading('AlexNet', 0)

    document.add_heading('Первая победившая свёрточная сеть', level=1)
    p = document.add_paragraph('Победила в соревновании LSVRC-2012 с ошибкой 15.3%,\n'
                               'против ошибки 26.2% на втором месте', style='List Bullet')

    document.add_picture('results.png', width=Inches(5))

    document.add_heading('Строение', level=1)

    document.add_paragraph(
        'Решается задача классификации на 1000 классов.', style='List Bullet'
    )
    document.add_paragraph(
        'Архитектура состоит из пяти свёрточных слоев и трех полносвязных.', style='List Bullet'
    )
    document.add_paragraph(
        'За первыми двумя свёрточными слоями следует Max Pooling с пересекающимися окнами', style='List Bullet'
    )
    document.add_paragraph(
        'Свёрточные слои номер 3, 4, 5 связаны напрямую (без Max Pooling).', style='List Bullet'
    )
    document.add_paragraph(
        'После FC-слоев применяется Dropout с p = 0.5.', style='List Bullet'
    )

    document.add_picture('AlexNet.png', width=Inches(5))

    document.add_heading('Слои', level=1)

    records = (
        ('Conv', 'kernel=11x11, stride=4, activation=ReLU'),
        ('Max pooling', 'kernel=3x3, stride=2'),
        ('Conv', 'kernel=5x5, padding=2, activation=ReLU'),
        ('Max pooling', 'kernel=3x3, stride=2'),
        ('Conv', 'kernel=3x3, padding=1, activation=ReLU'),
        ('Conv', 'kernel=3x3, padding=1, activation=ReLU'),
        ('Conv', 'kernel=3x3, padding=1, activation=ReLU'),
        ('Max pooling', 'kernel=3x3, stride=2'),
        ('Fully connected layer', 'in=9216, out=4096, activation=ReLU'),
        ('Fully connected layer', 'in=4096, out=4096, activation=ReLU'),
        ('Fully connected layer', 'in=4096, out=1000'),
        ('Softmax', '')
    )

    table = document.add_table(rows=1, cols=2)
    hdr_cells = table.rows[0].cells
    hdr_cells[0].text = 'Тип слоя'
    hdr_cells[1].text = 'Параметры'
    for qty, id in records:
        row_cells = table.add_row().cells
        row_cells[0].text = str(qty)
        row_cells[1].text = id

    document.add_page_break()

    document.add_heading('Функции активации', level=1)
    document.add_paragraph(
        'В качестве функции активации используется ReLU.'
        'В то время в основном использовались tanh и sigmoid.'
        'В сравнении с tanh сеть с ReLU достигла ошибки 0.25% в 6 раз быстрее.'
        'Это связано с горизонтальными асимптотами у tanh и sigmoid, '
        'что замедляет сходимость градиентного спуска.', style='List Bullet'
    )
    document.add_picture('activations.png', width=Inches(5))

    document.save('AlexNet.docx')

    if platform.startswith('win'):
        convert('AlexNet.docx')
    elif platform == 'linux':
        system('libreoffice --convert-to pdf AlexNet.docx')

    unlink('AlexNet.docx')


if __name__ == '__main__':
    main()
